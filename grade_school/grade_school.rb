# Defines a School class class.
class School 
  attr_accessor :roster

  def initialize
    @roster = Hash.new { |hash, key|  hash[key] = [] }
  end

  def students(grade)
    # sorts the list of students at the corresponding grade
    @roster[grade].sort_by!{ |name| name.downcase }
  end

  def add(student, grade)
    # inserts the student at the end of the list of students of the corresponding grade
  	@roster[grade].insert((@roster[grade].length), student)
	end

  def students_by_grade
    output = []
    if @roster.empty?
      return output
    else
      #@roster = @roster.sort_by{ |k| k[:grade]}
      @roster.sort.map do |grade, students|
        { :grade => grade, :students => students.sort }
      end
    end
  end

end