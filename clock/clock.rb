# Defines a Clock class.
class Clock 
  attr_accessor :hours
  attr_accessor :minutes

  def initialize(hours, minutes)
    @total_minutes = hours * 60 + minutes
  end

  # prins out the clock, formats with 0 depending on magnitude
  def to_s
  	if minutes < 10 and hours < 10
    	return "0#{hours}:0#{minutes}"
    elsif minutes < 10
    	return "#{hours}:0#{minutes}"
    elsif hours < 10
    	return "0#{hours}:#{minutes}"
    else
    	return "#{hours}:#{minutes}"
    end
  end

  # get hours from total_minutes
  def hours
  	(@total_minutes / 60) % 24
  end

  # get minutes from total_minutes
  def minutes
  	@total_minutes % 60
  end

  # creates a new Clock object with hour and minute values
  def self.at(hours, minutes)
  	Clock.new(hours, minutes)
  end

  def +(minutes)
  	@total_minutes += minutes
  	return self
  end

  def ==(second_clock)
  	self.to_s == second_clock.to_s
  end
end